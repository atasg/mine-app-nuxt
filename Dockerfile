FROM node:16-alpine
# in latest version Webpack build failing with ERR_OSSL_EVP_UNSUPPORTED
RUN mkdir -p /app

COPY . /app
WORKDIR /app

ENV NODE_ENV=development

RUN npm install
RUN npm run build

EXPOSE 3000

ENV NUXT_HOST=0.0.0.0

ENV NUXT_PORT=3000

CMD [ "npm", "start" ]