export const state = () => ({
    score: 0,
    remainLife:3,
    lostLife:0,
    mine:{
        left:0,
        top:100
    },
    mineStyle:{
        left:"0px",
        top:"100px"
    }
})

export const getters = {
    score: state => state.score,
    remainLife: state => state.remainLife,
}

export const mutations = {
    increaseScore(state) {
        state.score++
    },
    pushedOnTheMine(state) {
        if (state.remainLife != 0) {
            state.remainLife--
        }

    },
    lostLife(state){
        state.lostLife = 3 - state.remainLife
    }
}